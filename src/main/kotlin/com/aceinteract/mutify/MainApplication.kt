package com.aceinteract.mutify

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import twitter4j.*
import twitter4j.conf.ConfigurationBuilder
import java.io.File


class MainApplication {

    companion object {

        @JvmStatic
        @Throws(Exception::class)
        fun main(args: Array<String>) {
            println("Hello Bot!")

            val gson = Gson()

            val jsonFile = File("tweets.json")
            if (!jsonFile.exists()) {
                jsonFile.createNewFile()
                jsonFile.writeText("[]")
            }

            val configurationBuilder = ConfigurationBuilder()
            configurationBuilder.setDebugEnabled(true)
                    .setOAuthConsumerKey("N4KGbSd4qSa7FZIIJsyFuSmlH")
                    .setOAuthConsumerSecret("OR80qqtvNADihfDVA160DrvC3PlPxphpNAbwY41AxterAEY2RX")
                    .setOAuthAccessToken("431424329-cN3OEx9SmgfE3ytt7Idjjqu1hrJHNn1MoOq1D8Eu")
                    .setOAuthAccessTokenSecret("GqIb2cS3AcW4I8GU04sbhMuNEpXeR0a9hSkr9xib3RwSC")

            val configuration = configurationBuilder.build()

            val twitterFactory = TwitterFactory(configuration)

            val twitter = twitterFactory.instance

            val listener: StatusListener = object : StatusListener {
                override fun onDeletionNotice(p0: StatusDeletionNotice?) {

                }

                override fun onScrubGeo(p0: Long, p1: Long) {

                }

                override fun onStatus(status: Status) {
                    val type = object : TypeToken<ArrayList<String>>() {}.type
                    val tweets = gson.fromJson<ArrayList<String>>(jsonFile.readText(), type)

                    println(status.user.name + " : " + status.text)

                    val originalStatus = twitter.showStatus(status.inReplyToStatusId)
                    tweets.add(originalStatus.text)
                    println("Got this: ${originalStatus.text}")

                    jsonFile.writeText(gson.toJson(tweets))

                    val statusReply = StatusUpdate("Marked 😉. Carry on").inReplyToStatusId(status.id)

                    val reply = twitter.updateStatus(statusReply)

                    println("Posted reply " + reply.id + " in response to tweet " + reply.inReplyToStatusId)

                }

                override fun onTrackLimitationNotice(numberOfLimitedStatuses: Int) {
                    println(numberOfLimitedStatuses.toString() + " were limited")
                }

                override fun onException(ex: Exception) {
                    ex.printStackTrace()
                }

                override fun onStallWarning(arg0: StallWarning) {
                    println("Connection stalled")
                }


            }

            val twitterStream = TwitterStreamFactory(configuration).instance
            twitterStream.addListener(listener)

            val filter = FilterQuery()
            filter.track("#tweetmutify")

            twitterStream.filter(filter)
        }

    }

}